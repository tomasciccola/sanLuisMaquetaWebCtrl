const choo = require('choo')
const devtools = require('choo-devtools')
const html = require('choo/html')
const css = require('sheetify')
const xhr = require('xhr')

const schema = require('./schemaZonas.json')

var app = choo()

app.use(devtools())
app.mount('body')

/* ------------------- STORE -------------------------- */
app.use((state, emitter) => {
  state.once = true
  state.serverRoot = 'http://10.75.33.101:8001/'
  state.apiPath = 'dev/accionMaqueta'
  state.schema = schema
  state.atModulo = 'general'
  state.active = false
  state.bot = false
  state.activacion = false

  // Init button states
  Object.keys(schema).forEach(function (modulo) {
    Object.keys(schema[modulo]).forEach(function (zona) {
      Object.keys(schema[modulo][zona]).forEach(function (efecto) {
        // Si es botón
        if (schema[modulo][zona][efecto]['button']) {
          // Si es uno solo o varios por accion
          if (schema[modulo][zona][efecto]['button'] !== 1) {
            schema[modulo][zona][efecto]['btnState'] =
              new Array(schema[modulo][zona][efecto]['button']).fill(false)
          } else {
            schema[modulo][zona][efecto]['btnState'] = false
          }
        } else {
          // Si es slider
          schema[modulo][zona][efecto]['val'] = 0
        }
      })
    })
  })
  emitter.on('activar', () => {
    state.active = !state.active
    var val = state.active ? 1 : 0
    state.active ? alert('Modo de control activado') : alert('Modo de control desactivado')
    emitter.emit('render')
    var url = `${state.serverRoot}modoInterno/${val}`
    console.log(url)
    xhr.get(url, function (err, res) {
      if (err) {
        console.error('ERROR', err)
      } else {
        console.log('RESPONSE', res.body)
      }
    })
  })

  emitter.on('activarBot', () => {
    state.bot = !state.bot
    var val = state.bot ? 1 : 0
    state.bot ? alert('Bot activado') : alert('Bot desactivado')
    emitter.emit('render')
    var url = `${state.serverRoot}modoAutomatico/${val}`
    xhr.get(url, function (err, res) {
      if (err) {
        console.error('ERROR', err)
      } else {
        console.log(res.body)
      }
    })
  })

  emitter.on('activacion', ()=>{
    state.activacion = !state.activacion
    var val = state.activacion ? 1 : 0
    state.bot ? alert('Activacion') : alert('Reseteada activación')
    emitter.emit('render')
    var url = `${state.serverRoot}modoActivacion/${val}`
    xhr.get(url, function(err,res){
      if(err) {
        console.error('ERROR', err)
      }else {
        console.log(res.body)
      }
    })
  })
  
  emitter.on('moduloSelected', (modulo) => {
    state.atModulo = modulo
    emitter.emit('render')
  })

  emitter.on('accion', (accion) => {
    if (state.active) {
      var val
      // Si no es slider
      if (!accion.valor) {
        var btnState
        // Si es una acción de muchos botones manda el idx de botón como valor
        if (accion.idx) {

          state.schema[state.atModulo][accion.zona][accion.efecto]['btnState'][accion.idx] =
      !state.schema[state.atModulo][accion.zona][accion.efecto]['btnState'][accion.idx]

          btnState = state.schema[state.atModulo][accion.zona][accion.efecto]['btnState'][accion.idx]
          var offset = state.schema[state.atModulo][accion.zona][accion.efecto]['button']
          val = btnState ? accion.idx : accion.idx + offset
        } else {
          state.schema[state.atModulo][accion.zona][accion.efecto]['btnState'] =
      !state.schema[state.atModulo][accion.zona][accion.efecto]['btnState']

          btnState = state.schema[state.atModulo][accion.zona][accion.efecto]['btnState']
          if(state.atModulo == "5" && accion.zona == 'brazosroboticos'){
            switch(accion.efecto){
              case '0':
                val = 1
                break
              case '1':
                val = 1
                break
              case '2':
                val = 1
                break
              case '3':
                val = 31
                break
              case '4':
                val = btnState ? 1 : 0
                break
	      case '5':
		val = btnState ? 1 : 0
		break
            }
          }else{
            val = btnState ? 1 : 0
          }
        }
      } else {
        state.schema[state.atModulo][accion.zona][accion.efecto]['val'] = accion.valor
        val = accion.valor
      }
      // Arma la URL
      var reqUrl = `${state.serverRoot}${state.apiPath}/${state.atModulo}/${accion.zona}/${accion.efecto}/${val}`
      console.log('URL', reqUrl)
      xhr.get(reqUrl, function (err, res) {
        if (err)console.error('ERROR', err)

        console.log('RESPONSE', res.body)
      })
      emitter.emit('render')
    } else {
      alert('El modo de control está desactivado')
    }
  })
})

/* ----------------- CSS ------------------------------ */
var style = css`

  * {
  font-size:1.2em;
  }

  :host {
    background:#1c1c1c;
    color:white;
    font-family:monospace;
    padding:10px;
    font-size:2rem;
  }

  .header {
    background:grey;
    display:flex;
    flex-wrap:wrap;
    margin:10px;
    padding:20px;
    border:3px solid aliceblue;
  }

  .header h1{
    font-size:1.9em;
    flex-grow:1;
    color:#1c1c1c;
  }


  .header button{
    background:#1c1c1c;
    color:white;
    font-size:0.6em;
    padding:10px;
    margin:10px;
    border:2px solid white;
  }

  .dropbtn {
    font-size:1em;
    background: none;
    color: white;
    border:2px solid grey;
    padding: 16px;
    min-width: 160px;
    cursor: pointer;
  }

  .dropdown {
    position: relative;
    display: inline-block;
    margin:20px;
  }

  .dropdown-content {
    display: none;
    position: absolute;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    font-size:1em;
  }

  .dropdown-content button {
    color: white;
    background:#1c1c1c;
    min-width: 160px;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    font-size:1.0em;
  }

  .dropdown-content button:hover {
    background: white;
    color:#1c1c1c;
  }

  .dropdown:hover .dropdown-content {
    display: block;
  }

  .dropdown:hover .dropbtn {
    background-color: white;
    color:#1c1c1c;
    border:2px solid aliceblue;
  }

  .controles{
    display:flex;
    flex-wrap:wrap;
  }

  .control {
    flex-grow:1;
    font-size:1.0em;
    margin:10px;
    padding:10px;
    background:grey;
    color:black;
    border:2px solid aliceblue;
  }

  .control button{
    font-size:0.7em;
    margin:10px;
    padding:10px;
    background:#1c1c1c;
    color:white;
    width:auto;
  }
  .control label {
    font-size:0.5em;
    margin-top:15px;
  }
  .control input{
    margin:10px;
    margin-top:10px;
    padding:10px;
  }
  .ctrlGroup {
    background: #3c3c3c; 
  }
  .ctrlGroup button {
    font-size:0.5em;
  }

  footer {
    text-align:center;
    margin:20px;
    font-size:0.5em;
  }
`
app.route('/', (state, emit) => {
  return html`
  <body class=${style}>
    <div class=header>
    <h1> WEB CONTROL MAQUETA </h1>
    <button onclick=${click}>${state.active ? 'Desactivar Control' : 'Activar Control'}</button>
    <button onclick=${clickBot}>${state.bot ? 'Desactivar Bot' : 'Activar Bot'}</button>
    <button onclick=${clickActivacion}>${state.activacion ? 'Resetear Activación' : 'ACTIVACIÓN'}</button>
    </div>
    <div class=dropdown>
      <button class=dropbtn>MÓDULO</button>
      <div class=dropdown-content>
        ${Object.keys(schema).map(buildModuloNav)}
      </div>
    </div>
    <div class=controles>
    ${Object.keys(schema[state.atModulo]).map(buildZona)}
    </div>
    <footer> ~~ Mientras no se active el control, no se enviarán mensajes a la maqueta ~~ </footer>
  </body>
  `

  function click () {
    emit('activar')
  }

  function clickBot () {
    emit('activarBot')
  }

  function clickActivacion(){
    emit('activacion')
  }

  function buildModuloNav (modulo) {
    var moduloName = Object.keys(state.schema[modulo]).join('/')
    return html`
      <button  onclick=${click}>${moduloName}</button>
    `
    function click () {
      emit('moduloSelected', modulo)
    }
  }
  function buildZona (zona) {
    return html`
      <div class=control>
        <h3>${zona}</h3>
        <div>
        ${Object.keys(schema[state.atModulo][zona]).map(buildEfecto)}
        </div>
      </div>
      `

    function buildEfecto (efecto) {
      var active = css`
        :host{
          border:2px solid red;
        }
        `
      var inactive = css`
        :host{
          border:2px solid white;
        }
        `
      //
      var slider = state.schema[state.atModulo][zona][efecto]['slider']
      var button = state.schema[state.atModulo][zona][efecto]['button']
      var nombre = state.schema[state.atModulo][zona][efecto]['nombre']

      return html`${slider ? buildSlider() : buildButtons()}`

      function buildButtons () {
        if (button !== 1) {
          return html`
            <div class=ctrlGroup>
              ${new Array(button).fill('').map(buildButton)}
            </div>`
        } else {
          return buildButton()
        }

        function buildButton (_, i) {
          var idx = i === undefined ? '' : (': ' + (i + 1))
          var btnState
          if (idx === '') {
            btnState = state.schema[state.atModulo][zona][efecto]['btnState']
          } else {
            btnState = state.schema[state.atModulo][zona][efecto]['btnState'][i + 1]
          }
          return html`
          <button class=${btnState ? active : inactive} data-state=${btnState} onclick=${click}>
          ${nombre + idx}
          </button>
        `
          function click (ev) {
            ev.preventDefault()
            emit('accion', { zona, efecto, idx: i + 1 })
          }
        }
      }
      function buildSlider () {
        var sliderVal = state.schema[state.atModulo][zona][efecto]['val']
        return html`
          <div style="display:inline-flex;">
          <input onchange=${onChange} 
          type="range" min="0" max="${slider}" steps="1" value=${sliderVal}>
          <label> ${nombre} </label>
          </div>
        `
        function onChange (e) {
          e.preventDefault()
          emit('accion', { zona, efecto, valor: e.target.value })
        }
      }
    }
  }
})
